# Clippy

Web based clipboard . tool.


## Build

```
docker build .
docker run -it -p
```


# Integration with CopyClip

View all of the history of existing clips.

```
cd ~/Library/Containers/com.fiplab.clipboard/Data/Library/Application\ Support/CopyClip
sqlite3 copyclip.sqlite
select * from ZCLIPPING;
```