from django.db import models


class Device(models.Model):

    name = models.CharField(max_length=64)


class Clip(models.Model):
    class Meta:
        ordering = ('-timestamp', )

    value = models.TextField()
    timestamp = models.DateTimeField(auto_now=True)

    def found_in_clipboard(self):
        return self.source_set.filter(action='r').count()

    def saved_to_clipboard(self):
        return self.source_set.filter(action='w').count()

    def action(self):
        return self.source_set.first().action

    def device(self):
        return self.source_set.first().device.name


class Source(models.Model):
    class Meta:
        ordering = ('-timestamp', )

    actions = (
        ('r', 'Read from Clipboard'),
        ('w', 'Write to Clipboard'),
    )

    clip = models.ForeignKey(Clip, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now=True)
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    action = models.CharField(max_length=1, choices=actions)


class Note(models.Model):

    text = models.TextField()
    clip = models.ForeignKey(Clip, on_delete=models.CASCADE)
