from django.conf import settings

from cryptography.fernet import Fernet
from cryptography.fernet import InvalidToken


def decode_token(encrypted_token):
    key = settings.FERNET_KEY.encode('ASCII')
    cipher_suite = Fernet(key)
    encrypted = encrypted_token.encode('ASCII')

    try:
        decrypted = cipher_suite.decrypt(encrypted).decode('ASCII')
    except InvalidToken as e:
        raise ValueError('Invalid Token') from e
    return decrypted


def encode_token(decrypted_token):
    key = settings.FERNET_KEY.encode('ASCII')
    cipher_suite = Fernet(key)
    decrypted = str(decrypted_token).encode('ASCII')
    encrypted = cipher_suite.encrypt(decrypted).decode('ASCII')
    return encrypted
