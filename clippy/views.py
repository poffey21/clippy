from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views import generic

from . import utils
from . import models

from logging import getLogger

logger = getLogger(__name__)


class HomeView(generic.TemplateView):
    template_name = 'clippy/home.html'

    def dispatch(self, request, *args, **kwargs):
        if 'device' not in request.session:
            return HttpResponseRedirect(reverse('register'))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['device'] = self.request.session['device']
        context['device_name'] = self.request.session['device_name']
        return context


class ClipListView(generic.ListView):
    model = models.Clip

    def dispatch(self, request, *args, **kwargs):
        if 'device' not in request.session:
            return HttpResponseRedirect(reverse('register'))
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()
        if 'search' in self.request.GET:
            logger.info('filtering queryset via search.')
            qs = qs.filter(value__icontains=self.request.GET.get('search', ''))
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        device_pk = utils.decode_token(self.request.session['device'])
        context['device'] = self.request.session['device']
        context['device_name'] = self.request.session['device_name']
        most_recent = self.model.objects.filter(source__device__pk=device_pk).first()

        context['search_value'] = self.request.GET.get('search', '')
        if most_recent:
            context['most_recent'] = most_recent.value
        return context


class DeviceCreateView(generic.CreateView):
    model = models.Device
    success_url = reverse_lazy('home')
    fields = '__all__'

    def dispatch(self, request, *args, **kwargs):
        if 'device' in request.session:
            logger.info('Session was already found.')
            return HttpResponseRedirect(reverse('home'))
        logger.info('Session was not found.')
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        logger.info('Form was valid.')
        response = super().form_valid(form)
        self.request.session['device'] = utils.encode_token(self.object.pk)
        self.request.session['device_name'] = self.object.name
        return response
