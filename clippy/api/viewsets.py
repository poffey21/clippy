from rest_framework import viewsets

from . import serializers
from .. import models


class ClipViewSet(viewsets.ModelViewSet):
    queryset = models.Clip.objects.all()
    serializer_class = serializers.ClipSerializer
    http_method_names = ['get', 'post', ]


class DeviceViewSet(viewsets.ModelViewSet):
    queryset = models.Device.objects.all()
    serializer_class = serializers.DeviceSerializer
    http_method_names = ['get', 'post', 'put', '']


class SourceViewSet(viewsets.ModelViewSet):
    queryset = models.Source.objects.all()
    serializer_class = serializers.SourceSerializer
    http_method_names = ['get', 'post', 'put', '']
