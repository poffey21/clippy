# Serializers define the API representation.
from django.utils import timezone

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from rest_framework import serializers, fields

from .. import models
from .. import utils



class ClipValueSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = models.Clip
        fields = ('value', )


class ClipSerializer(serializers.HyperlinkedModelSerializer):
    action = fields.ChoiceField(choices=models.Source.actions)
    device = fields.CharField()

    class Meta:
        model = models.Clip
        fields = ('value', 'action', 'device')

    def save(self, **kwargs):
        try:
            device_pk = utils.decode_token(self.data.get('device', ''))
        except ValueError:
            return
        try:
            device = models.Device.objects.get(pk=device_pk)
            clip, created = models.Clip.objects.update_or_create(
                value=self.data['value'],
                defaults=dict(
                    timestamp=timezone.now()
                )
            )
        except (ObjectDoesNotExist, MultipleObjectsReturned):
            return
        action = self.data['action']
        models.Source.objects.create(
            device=device,
            clip=clip,
            action=action
        )
        return clip


class SourceSerializer(serializers.HyperlinkedModelSerializer):
    clip = ClipValueSerializer()

    class Meta:
        model = models.Source
        fields = ('clip', 'timestamp', 'device', 'action')


class DeviceSerializer(serializers.HyperlinkedModelSerializer):
    source_set = SourceSerializer(many=True)

    class Meta:
        model = models.Device
        fields = ('name', 'source_set')
