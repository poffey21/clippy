from rest_framework import routers

from . import viewsets

router = routers.DefaultRouter()
router.register(r'clips', viewsets.ClipViewSet)
router.register(r'devices', viewsets.DeviceViewSet)
router.register(r'sources', viewsets.SourceViewSet)
