# Generated by Django 2.2.2 on 2019-06-04 18:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Clip',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.TextField()),
                ('timestamp', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ('-timestamp',),
            },
        ),
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('action', models.CharField(choices=[('r', 'Read from Clipboard'), ('w', 'Write to Clipboard')], max_length=1)),
                ('clip', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clippy.Clip')),
                ('device', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clippy.Device')),
            ],
            options={
                'ordering': ('-timestamp',),
            },
        ),
    ]
