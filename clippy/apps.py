from django.apps import AppConfig


class ClippyConfig(AppConfig):
    name = 'clippy'
