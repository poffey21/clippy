from rest_framework import viewsets

from . import serializers
from .. import models


class ClippingViewSet(viewsets.ModelViewSet):
    queryset = models.Clipping.objects.all()
    serializer_class = serializers.ClippingSerializer
    http_method_names = ['get', 'post', ]

