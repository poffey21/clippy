# Serializers define the API representation.
from rest_framework import serializers, fields

from .. import models


class ClippingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Clipping
        fields = '__all__'
