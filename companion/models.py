from django.db import models


class Clipping(models.Model):
    class Meta:
        managed = False
        db_table = 'ZCLIPPING'
        ordering = ('-pk',)

    id = models.IntegerField(primary_key=True, db_column='z_pk')
    source = models.IntegerField(db_column='zsource')
    contents = models.TextField(db_column='zcontents')
    timestamp = models.DateTimeField(db_column='zdaterecorded')
