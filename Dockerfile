FROM python:3.7

# Place Built Packages Up Here since they take longer
RUN pip install --no-cache-dir uwsgi cryptography

ENV PROJECT=clippy
ENV CONTAINER_HOME=/srv
ENV CONTAINER_PROJECT=$CONTAINER_HOME/$PROJECT
ENV DJANGO_SETTINGS_MODULE=www.settings
ENV PYTHONPATH=.:${PYTHONPATH}

WORKDIR $CONTAINER_PROJECT


RUN mkdir -p $CONTAINER_PROJECT

COPY requirements.txt $CONTAINER_PROJECT
RUN pip install --no-cache-dir -r $CONTAINER_PROJECT/requirements.txt

COPY uwsgi.ini $CONTAINER_PROJECT
COPY www       $CONTAINER_PROJECT/www
COPY $PROJECT  $CONTAINER_PROJECT/$PROJECT

RUN cd $CONTAINER_PROJECT && django-admin migrate && django-admin collectstatic --noinput

CMD uwsgi --ini $CONTAINER_PROJECT/uwsgi.ini

EXPOSE 8080
